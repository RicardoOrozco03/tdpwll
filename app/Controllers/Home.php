<?php
namespace App\Controllers;
use App\Models\mUsuariosR;
use App\Models\mDocentesR;
use App\Models\mCursosR;
class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}
	public function bienvenida()
	{
		return view('vBienvenida');
	}
	public function registro()
	{
		return view('vRegistro');
	}
	public function registro_docente()
	{
		return view('vRegistroDoc');
	}
		public function registro_cursos()
	{
		return view('vRegistroCursos');
	}
	public function eliminar()
	{
		return view('vEliminar');
	}

	public function insertarForm()
	{
		$mUsuariosR = new mUsuariosR();
		$usuarioNuevo = [
			"Nombre" => $_POST['Nombre'],
			"Apellidos" => $_POST['Apellidos'],
			"Fecha_Nacimiento" => $_POST['Fecha_de_Nacimiento'],
			"Correo_electronico" => $_POST['Correo_electronico'],
			"Contraseñas" => $_POST['Contraseñas']
		];
		$mUsuariosR->insert($usuarioNuevo);
		$datoId['idRegistrado'] = $mUsuariosR->db->insertId();
		return view("vSuccess",$datoId);
		
	}

	public function InsertarDocentes()
	{
		$mDocentesR = new mDocentesR();
		$DocenteNuevo=[
		"nombre"=>$_POST['Nombre'],
		"apellidos"=>$_POST['Apellidos'],
		"correo"=>$_POST['Correo'],
		"contrasena"=>$_POST['Contrasena'],
		"carrera"=>$_POST['Carrera'],
		"telefono"=>$_POST['Telefono'],
		"domicilio"=>$_POST['Domicilio'],
		"residencia"=>$_POST['Residencia']
		];
		$mDocentesR->insert($DocenteNuevo);
		$datoId['idRegistrado'] = $mDocentesR->db->insertId();
		return view("vSuccess",$datoId);
	}
	
		public function InsertarCursos()
	{
		$mCursosR = new mCursosR();
		$CursoNuevo=[
		"nom_curso"=>$_POST['NombreCurso'],
		"clasificacion"=>$_POST['Clasificacion'],
		"duracion"=>$_POST['Duracion'],
		"costo"=>$_POST['Costo']
		];
		$mCursosR->insert($CursoNuevo);
		$datoId['idRegistrado'] = $mCursosR->db->insertId();
		return view("vSuccess",$datoId);
	}

 		public function mostrarRegistros()
	{
		$mUsuariosR = new mUsuariosR();
		$todos=$mUsuariosR->findAll();
		$usuarios= array('usuarios'=>$todos);
		return view("vRegistros",$usuarios);
	}

	 	public function mostrarDocentes()
	{
		$mDocentesR = new mDocentesR();
		$todos=$mDocentesR->findAll();
		$usuarios= array('usuarios'=>$todos);
		return view("vRegistroMDoc",$usuarios);
	}

		 public function mostrarCursos()
	{
		$mCursosR = new mCursosR();
		$todos=$mCursosR->findAll();
		$usuarios= array('usuarios'=>$todos);
		return view("vRegistrosMCur",$usuarios);
	}

		public function buscarRegistros()
	{
        $mUsuariosR = new mUsuariosR();
		$id_estudiante = $_POST['id_estudiante'];
		$estudiante = $mUsuariosR->find($id_estudiante);
		return view("vRegistroEncontrado",$estudiante);
	}
		
		/*public function actualizarRegistros()
	{
		$mUsuariosR = new mUsuariosR();
		$id_estudiante = $_POST['id_estudiante'];
		$usuarioActualizado =[
			"Nombre" => $_POST['Nombre'],
			"Apellidos" => $_POST['Apellidos'],
			"Fecha_Nacimiento" => $_POST['Fecha_Nacimiento'],
			"Correo_electronico" => $_POST['Correo_electronico']
			"Contraseñas" => $_POST['Contraseñas']
		];
		$mUsuariosR->update($id_estudiante,$usuarioActualizado);
		$Usuario=$mUsuariosR->find($id_estudiante);
		return $this->mostrarRegistros();
	}*/
	/*public function eliminarRegistro($id)
	{
		mUsuariosR = new mUsuariosR();
		$id_estudiante = $id;
		$mUsuariosR->delete($id_estudiante);

		return $this->mostrarRegistros();

	}*/
}

