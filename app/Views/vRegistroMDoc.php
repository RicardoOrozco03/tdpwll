<!DOCTYPE html>
<html>
<head>
	<title>Registros de los Docentes</title>
	  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body>
	<?php echo view('vHead.php'); ?>
	<div class="container">
		<div class="row">
			<h1>Registros Actuales De Los Docentes</h1>
			<table class="table table-striped">
				<thead>
					<tr>
					<th scope="col">ID</th>
					<th scope="col">Nombre</th>
					<th scope="col">Apellidos</th>
					<th scope="col">Fecha de Nacimiento</th>
					<th scope="col">Correo electronico</th>
					<th scope="col">Contraseña</th>	
					<th scope="col">Carrera</th>	
					<th scope="col">Telefono</th>	
					<th scope="col">Domicilio</th>
					<th scope="col">Residencia</th>	
					</tr>
			</thead>
			<tbody>
				<?php 
						$db = \Config\Database::connect();
						$query = $db->query("SELECT * FROM registro_docentes");
						foreach ($query->getResult('array') as $usuario) { ?>
							<center>
					<tr>
						<td><?php echo $usuario['id_doc']; ?></td>
						<td><?php echo $usuario['nombre']; ?></td>
						<td><?php echo $usuario['apellidos']; ?></td>
						<td><?php echo $usuario['correo']; ?></td>
						<td><?php echo $usuario['contrasena']; ?></td>
						<td><?php echo $usuario['carrera']; ?></td>
						<td><?php echo $usuario['telefono']; ?></td>
						<td><?php echo $usuario['domicilio']; ?></td>
						<td><?php echo $usuario['residencia']; ?></td>
					</tr>
							</center>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="mb-3 row">
		<button type="submit" style="color: #F8F9F9 ; background-color: #6C1635;border: #6C1635"  class="btn btn-primary mb-3">Actualizar</button>
		</div>
		<div class="d-grid gap-3">
  			<a class="btn btn-danger mb-3" href="<?php
            echo base_url(); ?>/Home/eliminarRegistro/<?php
            echo $id_estudiante; ?>">Eliminar</a>
		</div>
	</div>
</body>
	<?php echo view('vFooter.php');?>
</html>