<!DOCTYPE html>
<html>
<head>
	<?php echo view('vHead.php'); ?>
	<title>Registros</title>
</head>
<body>
	<div class="container">
	<div class="row">
		<h1> Registro encontrado </h1>

	<form method="POST" action="../Home/buscarRegistros">
		<input type="hidden" class="form-control" id="id_estudiante" name="id_estudiante" value="<?php echo $id_estudiante; ?>">
		
		<div class ="mb-3 row">
			<label for="Nombre" class="col-sm-2 col-form-label"> Nombre </label>
			<div class="col-sm-10">
				<input type="Nombre" class="form-control" id="Nombre" name="Nombre" value="<?php echo $Nombre; ?>">
			</div>
		</div>
		<div class ="mb-3 row">
			<label for="Apellidos" class="col-sm-2 col-form-label"> Apellidos </label>
			<div class="col-sm-10">
				<input type="text" class="form-control" id= "Apellidos" name= "Apellidos" value="<?php echo $Apellidos; ?>">	
			</div>
		</div>
		<div class ="mb-3 row">
			<label for="Fecha_Nacimiento" class="col-sm-2 col-form-label"> Fecha de Nacimiento </label>
			<div class="col-sm-10">
				<input type= "date" class="form-control" id= "Fecha_Nacimiento" name= "Fecha_Nacimiento" value="<?php echo $Fecha_Nacimiento; ?>">
			</div>
		</div>
		<div class ="mb-3 row">
			<label for="Correo_electronico" class="col-sm-2 col-form-label"> Email </label>
			<div class="col-sm-10">
				<input type= "text" class="form-control" id= "Correo_electronico" name= "Correo_electronico" value="<?php echo $Correo_electronico; ?>">
			</div>
		</div>
		<div class ="mb-3 row">
			<label for="Contraseñas" class="col-sm-2 col-form-label"> Password </label>
			<div class="col-sm-10">
				<input type="password" class="form-control" id= "Contraseñas" name= "Contraseñas" value="<?php echo $Contraseñas; ?>">
		</div>
		</div>
		<form method="POST" action="../Home/">
		<div class="mb-3 row">
		<button type="submit" style="color: #F8F9F9 ; background-color: #6C1635;border: #6C1635"  class="btn btn-primary mb-3">Actualizar</button>
		</div>
		<div class="d-grid gap-2">
  			<a type="button" class="btn btn-danger mb-3" href="<?php
            echo base_url(); ?>/Home/eliminarRegistro/<?php
            echo $id_estudiante; ?>">Eliminar</a>
  			"></a>
		</div>

	</form>
 </form>
  </div> 
	</div>
</body>
<?php echo view('vFooter.php');?>
</html>