<!DOCTYPE html>
<html>
<head>
	<title>BIENVENIDOS</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
			<center><h1>BIENVENIDO</h1></center>
	
<?php echo view('vHead.php');?>
</div>
</head>
<body>
  
  <br>
<div class="card mb-3">
  <font face="Couriel New" size="9"><h4 class="card-title">Curso de HTML</h4></font>
  <center>
  <img src="<?php echo base_url('imagenes/HTML.jpg'); ?>" width="580" height="350" >
  </center>
  <div class="card-body">
    
    <font size="5" face="Arial" color="#E7AD10">
    <p class="card-text">Este es el tutorial más completo de HTML y html5 que puedes encontrar, con lecciones ordenadas cuidadosamente aprenderás el lenguaje más básico de la web; HTML es sin duda el lenguaje por donde debes empezar para convertirte en un experto desarrollador web.</p>
  
  </font>
  </div>
</div>
<div class="card">
  <font face="Couriel New" size="9"><h4 class="card-title">Curso de CSS</h4></font>
  <center>
  <img src="<?php echo base_url('imagenes/CSS.jpg'); ?>" width="580" height="350" >
 </center>
  <div class="card-body">
    <font size="5" face="Arial" color="#E7AD10">
    <p class="card-text">CSS (Cascade Style Sheet) son un conjunto de instrucciones que definen la apariencia de diversos elementos de un documento HTML. El HTML posee ciertas limitaciones a la hora de aplicarle forma a un documento. Pero con estas somos capaces de superar esas limitaciones. Las CSS complementan al lenguaje HTML, dándole a éste mayores posibilidades, por lo que si ya has realizado el tutorial de HTML, te recomendamos que sigas con nuestros manuales y tutoriales de CSS.
    Para crear una hoja de estilo basta con usar cualquier editor de texto, por ejemplo, el mismo bloc de notas. Uno de los más conocidos es el Dreamweaver, de Adobe, pero puedes usar cualquier otro.

    En este curso de CSS hemos añadido varios ejemplos prácticos de cada estilo. Algunos estilos se usan en portales como I-Decoracion u otras páginas. Te recomendamos que aprendas la teoría, luego mires el ejemplo y después intentes reproducirlo tu mismo.</p>
  </font>
  </div>
</div>
</font>
</center>
</font>
</body>
<?php echo view('vFooter.php');?>

</html>