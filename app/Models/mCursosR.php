<?php
namespace App\Models;
use CodeIgniter\Model;
class mCursosR extends Model{
	protected $table = 'cursos';
	protected $primarykey ='id_curso';

	protected $useAutoIncremet=true;

	protected $returnType ='array';
	protected $useSoftDeletes = true;

	protected $allowedFields = ['nom_curso', 'clasificacion','duracion','costo'];

	protected $useTimestamps = false;
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
	protected $deletedField  = 'deleted_at';

	protected $validationRules = [];
	protected $validationMessages = [];
	protected $skipValidation = false;	
	}	
?>