-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 19-03-2021 a las 05:31:03
-- Versión del servidor: 8.0.21
-- Versión de PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `estudiantes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

DROP TABLE IF EXISTS `cursos`;
CREATE TABLE IF NOT EXISTS `cursos` (
  `id_curso` int NOT NULL AUTO_INCREMENT,
  `nom_curso` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `clasificacion` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci NOT NULL,
  `duracion` int NOT NULL,
  `costo` int NOT NULL,
  PRIMARY KEY (`id_curso`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id_curso`, `nom_curso`, `clasificacion`, `duracion`, `costo`) VALUES
(1, 'matematicas basicas', 'matematicas', 10, 15000),
(3, 'Html en 5 seg', 'WEB', 30, 500000000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insertar_estudiantes`
--

DROP TABLE IF EXISTS `insertar_estudiantes`;
CREATE TABLE IF NOT EXISTS `insertar_estudiantes` (
  `id_estudiante` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `Apellidos` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `Fecha_Nacimiento` date NOT NULL,
  `Correo_electronico` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Contraseñas` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id_estudiante`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `insertar_estudiantes`
--

INSERT INTO `insertar_estudiantes` (`id_estudiante`, `Nombre`, `Apellidos`, `Fecha_Nacimiento`, `Correo_electronico`, `Contraseñas`) VALUES
(1, 'Antonio ', 'Avila', '2021-03-02', 'pepin@itesg.com', '1234pepin'),
(2, 'Ricardo', 'Briones', '2020-10-14', '1234@hotmail', '1234'),
(3, 'Ricardo', 'Briones', '2020-10-14', '1234@hotmail', '1234'),
(4, 'Ricardo', 'Briones', '2020-10-14', '1234@hotmail', '1234'),
(5, 'Ricardo', 'Briones', '2020-10-14', '1234@hotmail', '1234'),
(6, 'Ricardo', 'Briones', '2020-10-14', '1234@hotmail', '1234'),
(7, 'Ricardo', 'Orozco', '2021-04-08', '17111116@tecguanajuato.edu.mx', '1234'),
(8, 'Ricardo', 'Orozco', '2021-04-08', '17111116@tecguanajuato.edu.mx', '1234'),
(9, 'Ricardo', 'Orozco', '2021-04-08', '17111116@tecguanajuato.edu.mx', '1234'),
(10, 'Ricardo', 'Briones', '2021-03-09', 'ricardoorozcob99@hotmail.com', '12345'),
(11, 'Jorge Alberto', 'Lopez', '2020-06-25', '17111116@tecguanajuato.edu.mx', '12345');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_docentes`
--

DROP TABLE IF EXISTS `registro_docentes`;
CREATE TABLE IF NOT EXISTS `registro_docentes` (
  `id_doc` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `apellidos` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `correo` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `contrasena` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `carrera` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `telefono` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `domicilio` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `residencia` varchar(200) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_doc`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Volcado de datos para la tabla `registro_docentes`
--

INSERT INTO `registro_docentes` (`id_doc`, `nombre`, `apellidos`, `correo`, `contrasena`, `carrera`, `telefono`, `domicilio`, `residencia`) VALUES
(1, 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
(2, 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
(3, 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
(4, 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
(5, 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
(6, 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A'),
(7, 'Jorge Alberto', 'Barboza', 'yorch.lopez.dwn@gmail.com', '12345', '', '+525584705609', 'Domicilio 1', 'Guanajuato'),
(8, 'pepin', 'yuyin', 'tato@hotmail.com', '1234', 'Sistemas', '473426378', 'santa teresa', 'Guanajuato');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
